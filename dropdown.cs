﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropdown : MonoBehaviour
{
    List<string> m_DropOptions = new List<string> { "Body", "Head", "chest", "Right Hand", "left Hand", "right Leg", "Left Leg" };
    //This is the Dropdown
    Dropdown m_Dropdown;
    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Dropdown GameObject the script is attached to
        m_Dropdown = GetComponent<Dropdown>();
        //Clear the old options of the Dropdown menu
        m_Dropdown.ClearOptions();
        //Add the options created in the List above
        m_Dropdown.AddOptions(m_DropOptions);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
